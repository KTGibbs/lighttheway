{
    "id": "dff32242-5398-43b7-984d-1b0ca5a50a56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d2fd923-f6bf-48cc-9290-7ac38978f835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dff32242-5398-43b7-984d-1b0ca5a50a56",
            "compositeImage": {
                "id": "9496b20c-bfce-4ef7-9510-c6279e7eb446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d2fd923-f6bf-48cc-9290-7ac38978f835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cb58365-29b6-4785-8470-e4b37fed2684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d2fd923-f6bf-48cc-9290-7ac38978f835",
                    "LayerId": "f7928c9d-771b-4f54-bb86-47e39719dab4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "f7928c9d-771b-4f54-bb86-47e39719dab4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dff32242-5398-43b7-984d-1b0ca5a50a56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}