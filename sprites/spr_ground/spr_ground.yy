{
    "id": "d3d388de-d096-4501-ab94-a2b716832042",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a670545-2e43-4cce-b36c-cc7b9b037969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3d388de-d096-4501-ab94-a2b716832042",
            "compositeImage": {
                "id": "89a2d041-ffdb-4dbc-bff0-3a29b269de4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a670545-2e43-4cce-b36c-cc7b9b037969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4af8397-e2bb-4e76-b8dc-e099fc9714ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a670545-2e43-4cce-b36c-cc7b9b037969",
                    "LayerId": "3e3735b6-aa5b-4a02-97cb-b635908037bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3e3735b6-aa5b-4a02-97cb-b635908037bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3d388de-d096-4501-ab94-a2b716832042",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}