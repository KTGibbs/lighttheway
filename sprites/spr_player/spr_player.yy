{
    "id": "db8cf229-789a-40cd-bd2a-be6d2618043f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ad5874e-5eff-493e-a533-1232f0b1c815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db8cf229-789a-40cd-bd2a-be6d2618043f",
            "compositeImage": {
                "id": "f45f47d9-5698-4bd4-9182-97e25d428b01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ad5874e-5eff-493e-a533-1232f0b1c815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39f73a5f-2658-471a-85a1-446b37879592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ad5874e-5eff-493e-a533-1232f0b1c815",
                    "LayerId": "2f989090-7381-40ba-a0ff-856cf1df9179"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2f989090-7381-40ba-a0ff-856cf1df9179",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db8cf229-789a-40cd-bd2a-be6d2618043f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -36,
    "yorig": -77
}