key_jump = keyboard_check_pressed(vk_space);
key_left = keyboard_check(ord("A"));
key_right = keyboard_check(ord("D"));
key_down = keyboard_check_pressed(ord("S"));

if(key_left){xspeed += -10}
if(key_right){xspeed += 10}

//Cap Xspeed at 10 and -10
if(xspeed > 10){xspeed = 10}
if(xspeed < -10){xspeed = -10}
if(yspeed > 50){yspeed = 50}

x += xspeed;
y += yspeed;

xspeed = 0;


#region Ground/Gravity Stuff
if(place_meeting(x,y+1,o_ground)){
	onGround = true;
	grav = 1.5;
}

else{
	onGround = false;
}

if(!onGround && key_down){
	grav = 10;
}

if(onGround && key_jump){
	key_down = false;
	yspeed = -jumpspeed;
	doubleJump = true;
	key_jump = false;
}

if(doubleJump && key_jump){
	yspeed = -jumpspeed;
	doubleJump = false;
}


yspeed += grav;
#endregion

#region Horizontal/Vertical Collision
if(place_meeting(x+xspeed, y, o_ground)){
	while(!place_meeting(x+sign(xspeed),y,o_ground)){
		x = x + sign(xspeed);
	}
	xspeed = 0;
}

if(place_meeting(x, y+yspeed, o_ground)){
	while(!place_meeting(x,y+sign(yspeed),o_ground)){
		y = y + sign(yspeed);
	}
	yspeed = 0;
}
#endregion


