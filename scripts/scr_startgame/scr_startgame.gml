instance_create_depth(0,0,0,o_controller);
instance_create_depth(mouse_x,mouse_y,0,o_cursor);
instance_create_depth(0,0,0,o_bg);
instance_create_depth(450,200,-1,o_player);

for(var i = 0; i < 10; i++){
	instance_create_depth(i*64 + 200,400,-2,o_ground);
}
